Given('que eu acesso a página principal') do
    visit 'https://foundation.e-core.com/login/index.php'
end

When('eu faço login com {string} e {string}') do |email, senha|
    find('input[id=username]').set email
    find('#password').set senha
    click_button 'Acessar'
end

Then('devo ser autenticado com sucesso') do
    expect(page).to have_content 'Administrador'
end

Then('devo pesquisa {string}') do |mensagem|
    click_button 'Cadastre-se gratuitamente'
end

Then('devo ver a seguinte mensagem {string}') do |mensagem|
    expect(page).to have_content mensagem
end
